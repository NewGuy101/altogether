Tasks to answer in your own README.md that you submit on Canvas:

1.  See logger.log, why is it different from the log to console?  
    We want to have a handler for the file we are writing to, something that Console logging woundn't help with.
	
2.  Where does this line come from? FINER org.junit.jupiter.engine.execution.ConditionEvaluator logResult Evaluation of condition [org.junit.jupiter.engine.extension.DisabledCondition] resulted in: ConditionEvaluationResult [enabled = true, reason = '@Disabled is not present']  
    This comes from the ConditionEvaluator class under the org.junit.jupiter.engine.execution package, which is a part of the Mavin Dependencies.

3.  What does Assertions.assertThrows do?  
    Asserts that execution of the supplied executable throws an exception of the Timer class and returns the Timer exception.

4.  See TimerException and there are 3 questions

    1.  What is serialVersionUID and why do we need it? (please read on Internet)  
    The serialVersionUID is used during deserialization to verify that the sender and receiver of a serialized object have loaded classes for that object that are compatible with respect to serialization. We need it because otherwise one will be provided by the compiler, which can be different for each compiler and class.
    
    2.  Why do we need to override constructors?  
    We need to over ride this bacause we do not inherit nondefault constructors
    
    3.  Why we did not override other Exception methods?  	
    Because we don't want them or won't be using them.
    
5.  The Timer.java has a static block static {}, what does it do? (determine when called by debugger)  
    The static block is used for initializing our static variables. Static gets executed when the Timer class is loaded in the memory.

6.  What is README.md file format how is it related to bitbucket? (https://confluence.atlassian.com/bitbucketserver/markdown-syntax-guide-776639995.html)  
    READ.md uses Markdown as its file formatter and is what bitbucket uses to display your repository's contents on the overview page

7.  Why is the test failing? what do we need to change in Timer? (fix that all tests pass and describe the issue)  
    The test is failing because the time to wait for the fail test is negative, which means that timenow is still null, and throws a NullPointerException when we reach the finally block. We can fix the issue by making it so that the contents inside the finally block only run when the timeToWait >= 0.

8.  What is the actual issue here, what is the sequence of Exceptions and handlers (debug)  
    After the fix, we get only a TimerException

9.  Make a printScreen of your eclipse JUnit5 plugin run (JUnit window at the bottom panel) 
    ![alt text](https://bytebucket.org/NewGuy101/altogether/raw/2b3b2c0cc9a9933e707b92ceb09e044f000afd7d/JUnit.JPG "JUnit")
    
10. Make a printScreen of your eclipse Maven test run, with console
    ![alt text](https://bytebucket.org/NewGuy101/altogether/raw/a27c27b763085bd24fc3c7a9555e21fa5d1b9491/Maven.JPG "Maven")

11. What category of Exceptions is TimerException and what is NullPointerException?  
    TimerException is a checked exception while NullPointerException is an unchecked exception.

12. Push the updated/fixed source code to your own repository.  
    https://bitbucket.org/NewGuy101/altogether